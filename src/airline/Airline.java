package airline;

public class Airline {
    
    private String name;
    private int airlineCode;
        
    public Airline(String name, int airlineCode){
        setName(name);
        setAirlineCode(airlineCode);
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public int getAirlineCode(){
        return airlineCode;
    }
    
    public void setAirlineCode(int airlineCode){
        this.airlineCode = airlineCode; 
    }
}
