package airline;

import java.util.Date;

public class AirlineSimulation {

    public static void main(String[] args) {
                
        Airline airline = new Airline("Air Sheridan", 5896); 
        
        Flight flight = new Flight("2579", new Date(2021, 07, 19, 12, 30, 0));
        
        Airplane airplane = new Airplane("Embracer", "172N", 7777, 84);
        
        Airport airport = new Airport("993528", "Los Angeles, California");  
        
        System.out.println(airline.getName() + " airline operates flight " + 
                flight.getNumber() + " that is flying out on " + 
                flight.getDate() + ". This flight is assigned to the " + 
                airplane.getMake() + ", model " + airplane.getModel() + 
                ". This airplane is leaving from the " + airport.getPortCode() 
                + " airport in " + airport.getLocation());
    }
}
