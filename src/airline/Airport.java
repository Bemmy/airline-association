package airline;


public class Airport {
    
    private String portCode;
    private String location;
    
    public Airport(String portCode, String location){
        
        setPortCode(portCode);
        setLocation(location);
    }
    
    public String getPortCode(){
        return portCode;
    }
    
    public void setPortCode(String portCode){
        this.portCode = portCode;
    }
    
    public String getLocation(){
        return location;
    }
    
    public void setLocation(String location){
        this.location = location; 
    }
}
