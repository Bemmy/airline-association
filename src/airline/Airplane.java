package airline;


public class Airplane {
    
    private String make;
    private String model;
    private int id;
    private int seatNum;
        
    public Airplane(String make, String model, int id, int seatNum){
        setMake(make);
        setModel(model);
        setId(id);
        setSeatNum(seatNum);
    }
    
    public String getMake(){
        return make;
    }
    
    public void setMake(String make){
        this.make = make;
    }
    
    public String getModel(){
        return model;
    }
    
    public void setModel(String model){
        this.model = model;
    }
    
    public int getId(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public int getSeatNum(){
        return seatNum;
    }
    
    public void setSeatNum(int seatNum){
        this.seatNum = seatNum;
    }
}
