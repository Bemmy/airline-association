package airline;

import java.util.Date;

public class Flight {
    
    private String number;
    private Date date;
    
    public Flight(String number, Date date){
        setNumber(number);
        setDate(date);
    }
    
    public String getNumber(){
        return number;
    }
    
    public void setNumber(String code){
        this.number = code;
    }
    
    public Date getDate(){
        return date;
    }
    
    public void setDate(Date date){
        this.date = date; 
    }
}
